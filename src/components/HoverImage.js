import * as React from "react";
import { useHover } from "@uidotdev/usehooks";
import Image from 'react-bootstrap/Image';
import hoverImageA from '../assets/images/hover-image1.png'
import hoverImageB from '../assets/images/hover-image2.png'
import hoverImageC from '../assets/images/hover-image3.png'
import hoverImageD from '../assets/images/hover-image4.png'
import hoverImageE from '../assets/images/hover-image5.png'
import hoverImageF from '../assets/images/hover-image6.png'
import hoverImageG from '../assets/images/hover-image7.png'
import hoverImageH from '../assets/images/hover-image8.png'
import hoverImageI from '../assets/images/hover-image9.png'
import hoverImageJ from '../assets/images/hover-image10.png'






function getRandomImage() {
  const images = [hoverImageA, hoverImageB, hoverImageC, hoverImageD, hoverImageE, hoverImageF, hoverImageG, hoverImageH, hoverImageI, hoverImageJ];
  return images[Math.floor(Math.random() * images.length)];
}


export default function HoverImage() {
  const [ref, hovering] = useHover();

  const backgroundImage = hovering ? getRandomImage() : hoverImageB


  return (
    <div fluid>
      <Image fluid src={backgroundImage} ref={ref}></Image>
    </div>
  );
}