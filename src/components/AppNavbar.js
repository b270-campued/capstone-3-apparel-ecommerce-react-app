import Container from 'react-bootstrap/Container';
import { Fragment, useContext } from 'react';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link, NavLink } from 'react-router-dom';
import logo from '../assets/images/logo.png'
import UserContext from '../UserContext';

export default function AppNavbar(){

	// State to store the user information stored in the login page.
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);

	const { user } = useContext(UserContext);

	return (

		<Navbar className="navBarContainer" expand="lg">
		    <Container fluid>
		        <Navbar.Brand as={Link} to="/">
		        	<img
    	              src={logo}
    	              width="50"
    	              height="50"
    	              className=""
    	              alt="Shoe Stop logo"
    	            />
		        </Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          	<Nav className="me-auto">
			            <Nav.Link as={NavLink} to="/" className="navBarLinks">Home</Nav.Link>
			            {
        	            	(user.isAdmin)
        	            	?
        	            	<Nav.Link as={ NavLink } to="/admin" end className="navBarLinks">Admin Dashboard</Nav.Link>
        	            	:
        	           		<Nav.Link as={ NavLink } to="/products" end className="navBarLinks">Products</Nav.Link>
        	            }
			            {(user.id !== null)
			            	?
			            	<Nav.Link as={NavLink} to="/logout" className="navBarLinks">Logout</Nav.Link>
			            	:
			            	<Fragment>
			            		<Nav.Link as={NavLink} to="/login" className="navBarLinks">Login</Nav.Link>
			            		<Nav.Link as={NavLink} to="/register" className="navBarLinks">Register</Nav.Link>
			            	</Fragment>
			            }
			            
		          	</Nav>
		        </Navbar.Collapse>
		     </Container>
		</Navbar>
	)
}