import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {
	// Consume the "user" state from the "User" context object.
	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [ email, setEmail ] = useState("");
	const [mobileNo, setMobileNo] = useState('');
	const [ password1, setPassword1 ] = useState("");
	const [ password2, setPassword2 ] = useState("");
	// Set to determine whether submit button is enabled or not
	const [ isActive, setIsActive ] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both passwords match
		if((email !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, mobileNo, password1, password2]);

	// Function to simulate user registration
	function registerUser(e) {
		
		// Prevents page redirection via form submission
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
		    method: "POST",
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        email: email
		    })
		})
		.then(res => res.json())
		.then(data => {

		    console.log(data);

		    if(data){

		    	Swal.fire({
		    		title: 'Duplicate email found',
		    		icon: 'error',
		    		text: 'Kindly provide another email to complete the registration.'	
		    	});

		    } else {

		    	fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
		    	    method: "POST",
		    	    headers: {
		    	        'Content-Type': 'application/json'
		    	    },
		    	    body: JSON.stringify({
		    	        email: email,
		    	        mobileNo: mobileNo,
		    	        password: password1
		    	    })
		    	})
		    	.then(res => res.json())
		    	.then(data => {

		    	    console.log(data);

		    	    if (data) {

		    	        // Clear input fields
		    	        setEmail('');
		    	        setMobileNo('');
		    	        setPassword1('');
		    	        setPassword2('');

		    	        Swal.fire({
		    	            title: 'Registration successful',
		    	            icon: 'success',
		    	            text: 'Welcome to Shoe Stop!'
		    	        });

		    	        // Allows us to redirect the user to the login page after registering for an account
						navigate("/login");

		    	    } else {

		    	        Swal.fire({
		    	            title: 'Something went wrong.',
		    	            icon: 'error',
		    	            text: 'Please try again.'   
		    	        });

		    	         
		    	        
		    	    }
		    	})
		    }
		})
	}

	return (
		(user.id !== null) 
		?
		(
		    (user.isAdmin)
		    ?
		    <Navigate to="/admin"/>
		    :
		    <Navigate to="/products"/>
		)
		:
			<Container className="pt-5 loginAndRegisterBg" style={{ width: '25%'}}>
					<h1> Sign Up</h1>
					<Form onSubmit={(e) => registerUser(e)}>

					    <Form.Group className="mb-3" controlId="userEmail">
					        <Form.Label>Email address</Form.Label>
					        <Form.Control 
					        	type="email" 
					        	placeholder="Enter email"
					        	required
					        	value={email}
					        	onChange={e => setEmail(e.target.value)}
					        />
					        <Form.Text className="text-muted">
					          We'll never share your email with anyone else.
					        </Form.Text>
					    </Form.Group>

					    <Form.Group controlId="mobileNo">
					        <Form.Label>Mobile Number</Form.Label>
					        <Form.Control 
					            type="text" 
					            placeholder="Enter Mobile Number"
					            value={mobileNo} 
					            onChange={e => setMobileNo(e.target.value)}
					            required
					        />
					    </Form.Group>

					    <Form.Group className="mb-3" controlId="password1">
					        <Form.Label>Password</Form.Label>
					        <Form.Control 
					        	type="password" 
					        	placeholder="Password"
					        	required
					        	value={password1}
					        	onChange={e => setPassword1(e.target.value)}
					        />
					    </Form.Group>
				      	
				      	<Form.Group className="mb-3" controlId="password2">
				      	    <Form.Label>Verify Password</Form.Label>
				      	    <Form.Control 
				      	    	type="password" 
				      	    	placeholder="Verify Password"
				      	    	required
				      	    	value={password2}
					        	onChange={e => setPassword2(e.target.value)}
				      	    />
				      	</Form.Group>

				      	<p> Already a user? <a href="/login">Login</a> </p>

				      {/*conditional renderred submit button based on isActive state*/}
				    	{isActive
				    		?
				    		<Button  variant="warning" type="submit" style={{ width: '100%'}}>Submit</Button>
				    		:
				    		<Button variant="secondary" type="submit" disabled style={{ width: '100%'}}>Submit</Button>
				    	}

					</Form>
			</Container>
	)
}