import Container from 'react-bootstrap/Container';
import { Row, Col } from 'react-bootstrap';
import HoverImage from '../components/HoverImage';
import React from 'react';
import Typewriter from "typewriter-effect";








export default function Home() {
	
	
	return (
		<Container fluid className="">
			<Row className="d-flex flex-wrap">
				<Col className="typeWriter">
					<Typewriter
		 				
		                onInit={(typewriter) => {
		                    typewriter
		                        .typeString("NEW RELEASES!")
		                        .pauseFor(500)
		                        .deleteAll()
		                        .typeString("GREAT DEALS!")
		                        .pauseFor(500)
		                        .deleteAll()
		                        .typeString("ONLY IN SHOE STOP")
		                        .start();
		                }}
		            />
				</Col>
				<Col className="d-flex flex-wrap">
					<HoverImage/>
				</Col>
			</Row>
		</Container>
	)
}